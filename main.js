const express = require("express");
const app = express();

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const cors = require("cors");
app.use(
  cors({
    origin: "*",
  })
);

const mysql = require("mysql");
const { request, response } = require("express");
/*
const Pool = require("pg").Pool;
const pool = new Pool({
  //connectionLimit: 20,
  user: "weatherApp",
  host: "localhost",
  database: "weatherLog",
  password: "Supersecurepassword123",
  port: 5432,
});
*/
const pool = mysql.createPool({
  connectionLimit: 100,
  host: "eu-cdbr-west-03.cleardb.net",
  user: "be0c24be7c997e",
  password: "98e5379b",
  database: "heroku_245dc3cafe9ce0d",
});

app.use(express.static(__dirname));

app.get("/", (request, response) => {
  response.sendFile(path.join(__dirname + "/index.html"));
});

app.get("/api/places", (request, response) => {
  const sql = `SELECT * FROM places;`;
  pool.getConnection((error, connection) => {
    pool.query(sql, (err, placesArray) => {
      connection.release();
      if (err) {
        throw err;
      }
      const places = {};
      for (let i = 0; i < placesArray.length; i++) {
        places[placesArray[i].name] = placesArray[i];
      }
      response.send(places);
    });
  });
});

app.post("/api/log", (request, response) => {
  const entry = {
    timestamp: new Date(request.body.timestamp),
    place: request.body.place,
    temperature: request.body.temperature,
    humidity: request.body.humidity,
    windspeed: request.body.windspeed,
    weatherCode: request.body.weatherCode,
  };

  const sql1 = `SELECT * FROM places WHERE name='${entry.place.name}' LIMIT 1;`;
  pool.getConnection((error, connection) => {
    pool.query(sql1, (err1, res1) => {
      if (err1) {
        throw err1;
      }
      if (res1 == []) {
        pool.query(
          `INSERT INTO places (latitude, longitude, name, imageUrl) 
          VALUES (${entry.place.latitude}, ${entry.place.longitude}, ${entry.place.name}, ${entry.place.imageUrl});`,
          (err, res) => {
            if (err) {
              throw err;
            }
          }
        );
      }

      const dateArray = entry.timestamp.toISOString().split("T");
      dateArray[1] = dateArray[1].substring(0, dateArray[1].length - 5);
      const timestampString = dateArray[0] + " " + dateArray[1];

      const sql2 = `INSERT INTO logs 
        (timestamp, placeId, temperature, humidity, windspeed, weatherCode) 
                VALUES ('${timestampString}', 
                ${res1[0].id}, 
                ${entry.temperature}, 
                ${entry.humidity}, 
                ${entry.windspeed}, 
                ${entry.weatherCode});`;
      pool.query(sql2, (err2, res2) => {
        connection.release();
        if (err2) {
          throw err2;
        }
        response.send(JSON.stringify(res2.insertId));
      });
    });
  });
});

app.get("/api/image/:filename", (request, response) => {
  response.sendFile(__dirname + "/images/" + request.params.filename);
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log("Listening on port: " + port);
});
