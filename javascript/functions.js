let activePlace;
const webLocation = "https://weatherbyvjeko.onrender.com";
const places = {};
const weathercodeIndex = {
  0: "Clear sky",
  1: "Mainly clear",
  2: "Partly cloudy",
  3: "Overcast",
  45: "Fog",
  48: "Depositing rime fog",
  51: "Light drizzle",
  53: "Moderate drizzle",
  55: "Dense drizzle",
  56: "Light freezing drizzle",
  57: "Dense freezing drizzle",
  61: "Slight rain",
  63: "Moderate rain",
  65: "Heavy rain",
  66: "Light freezing rain",
  67: "Heavy freezing rain",
  71: "Slight snow fall",
  73: "Moderate snow fall",
  75: "Heavy snow fall",
  77: "Snow grains",
  80: "Slight rain showers",
  81: "Moderate rain showers",
  82: "Violent rain showers",
  85: "Slight snow showers",
  86: "Heavy snow showers",
  95: "Thunderstorm",
  96: "Thunderstorm with slight hail",
  99: "Thunderstorm with heavy hail",
};

function togglePlacesDropdownList() {
  if ($("#placesDropdownMenu").css("display") == "none") {
    $("#placesDropdownMenu").show();
    $("#placesDropdownMenu").css("min-width", $("#placeButton").css("width"));
  } else {
    $("#placesDropdownMenu").hide();
  }
}

window.onclick = function (event) {
  if (!event.target.matches(".dropdownMenuButton")) {
    $("#placesDropdownMenu").hide();
  }
};

function changePlace(event) {
  const url = setPlace(places[$(event.target).text()]);
  httpGet(url).then((data) => {
    update(data);
  });
}

function setPlace(newPlace) {
  activePlace = newPlace;
  $("body").css("background-image", "url(" + newPlace.imageUrl + ")");
  $("#placeButton").text(newPlace.name);
  return buildUrl(newPlace.latitude, newPlace.longitude);
}

function buildUrl(latitude, longitude) {
  const url =
    "https://api.open-meteo.com/v1/forecast?latitude=" +
    latitude +
    "&longitude=" +
    longitude +
    "&hourly=relativehumidity_2m&current_weather=true&timezone=Europe%2FBerlin";
  return url;
}

function update(data) {
  const date = new Date();
  const hour = date.getHours();

  $("#temperature").text(`${data.current_weather.temperature}°C`);
  $("#humidity").text(`${data.hourly.relativehumidity_2m[hour]}%`);
  $("#weather").text(`${weathercodeIndex[data.current_weather.weathercode]}`);
  $("#windSpeed").text(`${data.current_weather.windspeed}km/h`);
  logData(date, data);
}

async function httpGet(url) {
  const response = await fetch(url);
  return await response.json();
}

async function logData(date, data) {
  const url = webLocation + "/api/log";
  const msgBody = {
    timestamp: date.getTime(),
    place: activePlace,
    temperature: data.current_weather.temperature,
    humidity: data.hourly.relativehumidity_2m[date.getHours()],
    windspeed: data.current_weather.windspeed,
    weatherCode: data.current_weather.weathercode,
  };
  fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(msgBody),
  }).then((res) => {
    if (res.ok) {
      console.log("Data logged successfuly.");
    } else {
      console.log("Failed to log data. Response: ", res);
    }
  });
}

function startInterval() {
  setInterval(function () {
    httpGet(buildUrl(activePlace.latitude, activePlace.longitude)).then(
      (data) => {
        update(data);
      }
    );
  }, 60000);
}
