httpGet(webLocation + "/api/places").then((res) => {
  $(document).ready(function () {
    Object.assign(places, res);

    let htmlList = "";
    const placeNames = Object.keys(places);
    for (let i = 0; i < placeNames.length; i++) {
      htmlList +=
        '<button onclick="changePlace(event)">' + placeNames[i] + "</button>";
    }
    $("#placesDropdownMenu").html(htmlList);

    const dataUrl = setPlace(places[placeNames[0]]);

    httpGet(dataUrl).then((data) => {
      update(data);
    });
    startInterval();
  });
});
