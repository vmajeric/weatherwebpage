class Place {
    constructor (latitude, longitude, name, imageUrl){
        this.latitude=latitude;
        this.longitude=longitude;
        this.name=name;
        this.imageUrl=imageUrl;
    }
}